public without sharing class POIMTriggerHandler {

    @future(Callout=true)
    public static void SendToMagento(String newListJSON, Boolean isDelete)
    {
        List<AcctSeedERP__Purchase_Order_Inventory_Movement__c> newList = (List<AcctSeedERP__Purchase_Order_Inventory_Movement__c>)JSON.deserialize(newListJSON,List<AcctSeedERP__Purchase_Order_Inventory_Movement__c>.class);
        String q ='Select id, Username__c, API_Key__c from Magento_Setting__mdt ';
        if(runningInASandbox)
        {
            q += 'WHERE DeveloperName = \'Sandbox\' limit 1';
        }
        else
        {
            q += 'WHERE DeveloperName = \'Production\' limit 1';
        }

        magentoSOAP.Port m = new magentoSOAP.Port();
        String sessionId = 'testtoken';
        if(Test.isRunningTest()){
            Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('login'));
        }
        sessionId = MagentoCalls.fetchSessionID();

        system.debug(sessionId);

        for(AcctSeedERP__Purchase_Order_Inventory_Movement__c poim:[Select id, AcctSeedERP__Warehouse__c,AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c, AcctSeedERP__Quantity__c, AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Location__r.Magento_Sync__c from AcctSeedERP__Purchase_Order_Inventory_Movement__c where ID in: newList AND AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Location__r.Magento_Sync__c = TRUE AND AcctSeedERP__Quantity__c >0 ALL ROWS])
        {
            Decimal quantity = isDelete ? poim.AcctSeedERP__Quantity__c  * -1: poim.AcctSeedERP__Quantity__c;
            system.debug('Quantity -- ' + String.valueof((Integer)quantity));
            if(Test.isRunningTest()){
                Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogInventoryStockItemUpdateDelta'));
            }
            m.catalogInventoryStockItemUpdateDelta(sessionId,poim.AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c,(Integer)quantity);

            String warehouse;
            if(poim.AcctSeedERP__Warehouse__c == 'Maple Logistics')
            {
                warehouse = '10';
            }
            else if(poim.AcctSeedERP__Warehouse__c == 'American Book Company')
            {
                warehouse = '8';
            }
            quantity=(Integer)quantity;
            system.debug('Quantity -- ' + String.valueof(quantity));
            if(Test.isRunningTest()){
                Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogInventoryStockItemUpdateErp'));
            }
            Integer response = m.catalogInventoryStockItemUpdateErp(sessionId,warehouse,poim.AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c,String.valueOf(quantity));
            system.debug('response = ' + response);
        }


    }

    public static Boolean runningInASandbox {
        get {
            if (runningInASandbox == null) {
                runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
            }
            return runningInASandbox;
        }
        set;
    }
}