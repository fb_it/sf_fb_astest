@IsTest(seeAllData=true)
private class InventoryMovementTriggerTests {

    @IsTest
    static void testInventoryMovementTriggers() {
   
        
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogInventoryStockItemUpdateDelta'));
       
        AcctSeed__GL_Account__c glAccount = TestUtils.createGLAccount('test gl', 'Revenue', true);
        AcctSeed__GL_Account__c glAccount2 = TestUtils.createGLAccount('test gl2', 'Revenue', false);
        AcctSeed__GL_Account__c glAccount3 = TestUtils.createGLAccount('test gl3', 'Expense', false);
        AcctSeed__GL_Account__c glAccount4 = TestUtils.createGLAccount('test gl4', 'Balance Sheet', false);
        TestUtils.createGLAccounts();
        AcctSeed__Accounting_Variable__c glVariable = TestUtils.createAccountingVariable('1');
        AcctSeed__Accounting_Variable__c glVariable2 = TestUtils.createAccountingVariable('2');
        AcctSeed__Accounting_Variable__c glVariable3 = TestUtils.createAccountingVariable('3');
        AcctSeed__Accounting_Variable__c glVariable4 = TestUtils.createAccountingVariable('4');

        Account acct = TestUtils.createAccount(glVariable.id);

        AcctSeed__GL_Account__c GL= new AcctSeed__GL_Account__c(name='123', AcctSeed__Type__c='Expense', AcctSeed__Sub_Type_1__c='Labor');
        insert GL;

        AcctSeed__GL_Account__c GL1= new AcctSeed__GL_Account__c(name='124', AcctSeed__Type__c='Balance Sheet',AcctSeed__Sub_Type_1__c='Assets');
        insert GL1;

        AcctSeed__GL_Account__c GL2= new AcctSeed__GL_Account__c(name='125', AcctSeed__Type__c='Revenue');
        insert GL2;


        AcctSeed__Accounting_Period__c ap = [Select id from AcctSeed__Accounting_Period__c where AcctSeed__Status__c = 'Open' Order by AcctSeed__Start_Date__c desc limit 1];//new AcctSeed__Accounting_Period__c();
//        ap.Name = '2019-12';
//        ap.AcctSeed__Start_Date__c = system.today().addDays(-7);
//        ap.AcctSeed__End_Date__c = system.today().addDays(30);
//        ap.AcctSeed__Status__c = 'Open';
//        insert ap;

        Id pricebookId = Test.getStandardPricebookId();


        AcctSeed__Billing_Format__c bf = new AcctSeed__Billing_Format__c();
        bf.AcctSeed__Type__c = 'Activity Statement';
        bf.AcctSeed__Visualforce_PDF_Page__c = 'BillingActivityStatementPDF';
        bf.AcctSeed__Default_Email_Template__c = 'Activity_Statement_Email_Template';

        insert bf;

        //AcctSeed__Ledger__c ledger2 = TestUtils.createLedger(pricebookid,'Test Ledger',glAccount.Id,bf.id);
       // insert ledger2;
       
       AcctSeed__Ledger__c q =[SELECT Id FROM AcctSeed__Ledger__c WHERE AcctSeed__Type__c='Transactional'];
       AcctSeed__Ledger__c ledger2=q;

        //Create Test products
        Map<String,Product2> prods = TestUtils.createProducts(glAccount2.id, glAccount3.id, glAccount4.id);

        AcctSeedERP__Warehouse__c wh = new AcctSeedERP__Warehouse__c(Name = 'test wh');
        insert wh;

        List<AcctSeedERP__Location__c> locs = new List<AcctSeedERP__Location__c>();
        locs.add(new AcctSeedERP__Location__c(Name = 'Finished Goods', AcctSeedERP__Warehouse__c = wh.Id, Magento_Sync__c = true));
        locs.add(new AcctSeedERP__Location__c(Name = 'NVIS Main Office', AcctSeedERP__Warehouse__c = wh.Id, Magento_Sync__c = true));
        insert locs;

        AcctSeedERP__Inventory_Balance__c bal1 = new AcctSeedERP__Inventory_Balance__c(AcctSeedERP__Warehouse__c = wh.Id, AcctSeedERP__Product__c = prods.get('Test Part').id, AcctSeedERP__Location__c = locs[0].Id, AcctSeedERP__Ledger__c=ledger2.id);
        insert bal1;

        //AcctSeedERP__Inbound_Inventory_Movement__c ibm = new AcctSeedERP__Inbound_Inventory_Movement__c(AcctSeedERP__Credit_GL_Account__c=GL2.id,AcctSeedERP__Unit_Cost__c=12,AcctSeedERP__Inventory_Balance__c = bal1.Id, AcctSeedERP__Type__c = 'Accounting', AcctSeedERP__Quantity__c = 10, AcctSeedERP__Ledger__c=ledger2.id);
        //insert ibm;

       // AcctSeedERP__Outbound_Inventory_Movement__c obm = new AcctSeedERP__Outbound_Inventory_Movement__c(AcctSeedERP__Debit_GL_Account__c=GL2.id,AcctSeedERP__Unit_Cost__c=12,AcctSeedERP__Inventory_Balance__c = bal1.Id, AcctSeedERP__Type__c = 'Accounting', AcctSeedERP__Quantity__c = 10, AcctSeedERP__Ledger__c=ledger2.id,AcctSeedERP__GL_Account_Variable_1__c=glVariable.id,AcctSeedERP__GL_Account_Variable_2__c=glVariable2.id, AcctSeedERP__GL_Account_Variable_3__c=glVariable3.id);
        //insert obm;


        List<Product2> ProdsToInsert = new List<Product2>();
//ProductCode='NI-20-0036'
        ProdsToInsert.add(new Product2(Name='tstprod',IsActive = true,ISBN_13digit__c='6667778889990',AcctSeed__Inventory_Product__c=true,
                AcctSeed__Expense_GL_Account__c=GL.ID, AcctSeed__Inventory_GL_Account__c=GL1.ID, AcctSeed__Inventory_Type__c='Purchased', AcctSeed__Revenue_GL_Account__c=GL2.id, AcctSeed__Unit_Cost__c=100.00,Manufacturers_Suggested_Retail_Price__c=42,Price__c=51,Price_CAN__c=51));

        insert ProdsToInsert;

        AcctSeedERP__Purchase_Order__c po = new AcctSeedERP__Purchase_Order__c(AcctSeedERP__Purchase_Order_Format__c = bf.id,AcctSeedERP__Ledger__c = ledger2.id,AcctSeedERP__Order_Date__c = date.today().addDays(-7), AcctSeedERP__Vendor__c = acct.Id);
        insert po;

        AcctSeedERP__Purchase_Order_Line__c pol = new AcctSeedERP__Purchase_Order_Line__c(AcctSeedERP__Unit_Price__c=12, AcctSeedERP__Purchase_Order__c = po.Id, AcctSeedERP__Quantity__c = 10, AcctSeedERP__Product__c = prods.get('Test Part').id, AcctSeedERP__GL_Account_Variable_2__c=glVariable2.id,AcctSeedERP__GL_Account_Variable_3__c=glVariable3.id);
        insert pol;

        //AcctSeedERP__Purchase_Order_Inventory_Movement__c poim = new AcctSeedERP__Purchase_Order_Inventory_Movement__c(AcctSeedERP__Inventory_Balance__c=bal1.id, AcctSeedERP__Unit_Cost__c=100.00, AcctSeedERP__Purchase_Order_Line__c=pol.id, AcctSeedERP__Movement_Date__c=date.today().addDays(-7), AcctSeedERP__Quantity__c=10,AcctSeedERP__GL_Account_Variable_1__c=glVariable.id,AcctSeedERP__GL_Account_Variable_2__c=glVariable2.id,AcctSeedERP__GL_Account_Variable_3__c=glVariable3.id);//new AcctSeedERP__Purchase_Order_Inventory_Movement__c();
        AcctSeedERP__Purchase_Order_Inventory_Movement__c poim = new AcctSeedERP__Purchase_Order_Inventory_Movement__c();
       // insert poim;
        poim.AcctSeedERP__Inventory_Balance__c = bal1.Id;
       poim.AcctSeedERP__Unit_Cost__c = 10;
       poim.AcctSeedERP__Purchase_Order_Line__c = pol.Id;
       poim.AcctSeedERP__Movement_Date__c = date.today().addDays(-7);
       poim.AcctSeedERP__Quantity__c = 10;
       poim.AcctSeedERP__GL_Account_Variable_1__c=glVariable.id;
       poim.AcctSeedERP__GL_Account_Variable_2__c=glVariable2.id;
       poim.AcctSeedERP__GL_Account_Variable_3__c=glVariable3.id;
       insert poim;
        AcctSeedERP__Purchase_Order_Inventory_Movement__c poim2 = poim.clone();
        insert poim2;

    }
}