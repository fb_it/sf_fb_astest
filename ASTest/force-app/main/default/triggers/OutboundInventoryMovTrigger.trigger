trigger OutboundInventoryMovTrigger on AcctSeedERP__Outbound_Inventory_Movement__c (after insert, after delete) {

    if(trigger.isInsert)
    {
        OutboundInventoryMovTriggerHandler.SendToMagento(JSON.serialize(trigger.new), false);
    }
    if(trigger.isDelete) { OutboundInventoryMovTriggerHandler.SendToMagento(JSON.serialize(trigger.old), true);}
}