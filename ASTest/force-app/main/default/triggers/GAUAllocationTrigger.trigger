trigger GAUAllocationTrigger on npsp__Allocation__c (after insert, after update, after delete) {
    if(Trigger.isInsert || Trigger.isUpdate){
        GAUAllocationTriggerHandler.rollupFunds(trigger.new, trigger.old,'Allocation',false);
        GAUAllocationTriggerHandler.rollupFunds(trigger.new, trigger.old,'Allocation',true);
        GAUAllocationTriggerHandler.rollupFunds(trigger.new, trigger.old,'Disbursement',false);
        
        OpportunityTriggerHandler.rollupFunds(trigger.new, trigger.old,'Allocation');
    }
    
    if(Trigger.isDelete){
        system.debug('GAUAllocationTrigger Delete');
        GAUAllocationTriggerHandler.rollupFunds(trigger.old,trigger.new,'Allocation',false);
        GAUAllocationTriggerHandler.rollupFunds(trigger.old,trigger.new,'Allocation',true);
        GAUAllocationTriggerHandler.rollupFunds(trigger.old,trigger.new,'Disbursement',false);
        
        OpportunityTriggerHandler.rollupFunds(trigger.old,trigger.new,'Allocation');
    }
}