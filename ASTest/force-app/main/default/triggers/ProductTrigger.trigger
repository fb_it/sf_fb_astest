trigger ProductTrigger on Product2 (before delete) {

    for(Product2 prod: Trigger.old){
        if (prod.Qty_on_Order__c!=null) {
            prod.addError('APEX ERROR: This Product currently exists on Purchase Order Lines.');
            
        }
    }

}