public without sharing class PurchaseOrderLineTriggerHandler {
    public static void updateVersionStatus(AcctSeedERP__Purchase_Order_Line__c pol, AcctSeedERP__Purchase_Order_Line__c polOld, String polPOId, Boolean isDelete)
    {
        AcctSeedERP__Purchase_Order__c po = [SELECT Id,Name,Version__c,AcctSeedERP__Status__c FROM AcctSeedERP__Purchase_Order__c WHERE Id = :polPOId];
        if (
                (
                    (
                        pol.AcctSeedERP__Quantity__c!=polOld.AcctSeedERP__Quantity__c ||
                        pol.AcctSeedERP__Unit_Price__c!=polOld.AcctSeedERP__Unit_Price__c ||
                        pol.Discount__c!=polOld.Discount__c ||
                        pol.Reorder_Carton_Quantity__c!=polOld.Reorder_Carton_Quantity__c ||
                        pol.Retail_Price__c !=polOld.Retail_Price__c
                    )
                    ||
                    isDelete
                )
                &&
                po.AcctSeedERP__Status__c=='Sent'
        ) {
            System.debug('pol= '+po.Name);
            System.debug('version= '+po.Version__c);
            if (po.Version__c!='Revision'||po.Version__c==null)
             {
                System.debug('updating...');
                po.Version__c='Revision';
                System.debug('now version= '+po.Version__c+' po id ='+po.Id);
                update po;
                
                //break;
                
            }
            
          }
    }
    public static void setVersionStatus(AcctSeedERP__Purchase_Order_Line__c pol, String polPOId)
    {
        AcctSeedERP__Purchase_Order__c po = [SELECT Id,Name,Version__c,AcctSeedERP__Status__c FROM AcctSeedERP__Purchase_Order__c WHERE Id = :polPOId];
        if (po.AcctSeedERP__Status__c=='Sent') 
            {
            System.debug('pol= '+po.Name);
            System.debug('version= '+po.Version__c);
            if (po.Version__c!='Revision'||po.Version__c==null)
             {
                System.debug('updating...');
                po.Version__c='Revision';
                System.debug('now version= '+po.Version__c+' po id ='+po.Id);
                update po;
                
                //break;
                
            }
            
            
          }
    }
}