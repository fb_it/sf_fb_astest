public with sharing class MagentoCalls {


    public static String fetchSessionID(){

        String q ='Select id, Username__c, API_Key__c from Magento_Setting__mdt ';
        if(runningInASandbox)
        {
            q += 'WHERE DeveloperName = \'Sandbox\' limit 1';
        }
        else
        {
            q += 'WHERE DeveloperName = \'Production\' limit 1';
        }

        Magento_Setting__mdt magentoSetting = Database.query(q);
        magentoSOAP.Port m = new magentoSOAP.Port();
        String sessionId='testtoken';
//        m.timeout_x = 9000;
        //if(!test.isRunningTest())
        //{
            system.debug(magentoSetting.Username__c);
            system.debug(magentoSetting.API_Key__c);
            if(Test.isRunningTest()){
                Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('login'));
            }
            sessionId = m.login(magentoSetting.Username__c,magentoSetting.API_Key__c);
        //}
        
        return sessionId;
    }

    public static String fetchAdminURL(){
        String q ='Select id, AdminURL__c from Magento_Setting__mdt ';
        if(runningInASandbox)
        {
            q += 'WHERE DeveloperName = \'Sandbox\' limit 1';
        }
        else
        {
            q += 'WHERE DeveloperName = \'Production\' limit 1';
        }

        Magento_Setting__mdt magentoSetting = Database.query(q);
        String adminURL=magentoSetting.AdminURL__c;        
        return adminURL;
    }

    public static Boolean runningInASandbox {
        get {
            if (runningInASandbox == null) {
                runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
            }
            return runningInASandbox;
        }
        set;
    }
}