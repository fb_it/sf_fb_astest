@isTest(SeeAllData=true)
public class TestUtils {

        //This method is from fflib
        public static Integer fakeIdCount = 0;
	public static final String ID_PATTERN = '000000000000';
        public static Id generateId(Schema.SObjectType sobjectType)
        {
                String keyPrefix = sobjectType.getDescribe().getKeyPrefix();
                fakeIdCount++;

                String fakeIdPrefix = ID_PATTERN.substring(0, 12 - String.valueOf(fakeIdCount).length());

                return Id.valueOf(keyPrefix + fakeIdPrefix + fakeIdCount);
        }    

    public static AcctSeed__Billing_Format__c createPOFormat(){
        AcctSeed__Billing_Format__c pof= new AcctSeed__Billing_Format__c();
        pof.Name='Test PO Format';
        pof.AcctSeed__Type__c='Purchase Order';
        pof.AcctSeed__Visualforce_PDF_Page__c='PurchaseOrderPDF';
        pof.AcctSeed__Default_Email_Template__c='Purchase_Order_Email_Template';

        insert pof;
        return pof;

    }

    public static Account createAccount(Id glAccount1){


        Account acct = new Account();
        acct.Name = 'Test Account';
        acct.AcctSeed__GL_Account_Variable_1__c = glAccount1;
        acct.BillingStreet = '395 Piedmont Ave NE';
        acct.BillingCity = 'Atlanta';
        acct.BillingState = 'Georgia';
        acct.BillingPostalCode = '30308';
        acct.BillingCountry = 'United States';


        insert acct;

        return acct;

    }

    public static Account createVendor(Id glAccount1){


        Account acct = new Account();
        acct.Name = 'TEST - Steve Vendor';
        acct.AcctSeed__GL_Account_Variable_1__c = glAccount1;
        acct.BillingStreet = '395 Piedmont Ave NE';
        acct.BillingCity = 'Atlanta';
        acct.BillingState = 'Georgia';
        acct.BillingPostalCode = '30308';
        acct.BillingCountry = 'United States';
        acct.AcctSeed__Accounting_Type__c='Vendor';

        insert acct;

        return acct;

    }
    public static Account createChildVendor(Id glAccount1, String parentId){


        Account acct = new Account();
        acct.Name = 'TEST - Steve Child Vendor';
        acct.AcctSeed__GL_Account_Variable_1__c = glAccount1;
        acct.BillingStreet = '395 Piedmont Ave NE';
        acct.BillingCity = 'Atlanta';
        acct.BillingState = 'Georgia';
        acct.BillingPostalCode = '30308';
        acct.BillingCountry = 'United States';
        acct.AcctSeed__Accounting_Type__c='Vendor';
        acct.ParentId=parentId;
        acct.Top_Level_Parent__c=parentId;
        //acct.Own_PO__c=TRUE;

        insert acct;

        return acct;

    }

    public static AcctSeed__GL_Account__c createGLAccount(String name, String type, Boolean isBank)
    {
        AcctSeed__GL_Account__c glAccount = new AcctSeed__GL_Account__c();
        glAccount.Name = name;
        glAccount.AcctSeed__Type__c = type;
        glAccount.AcctSeed__Bank__c = isBank;
        if(type =='Balance Sheet')
            glAccount.AcctSeed__Sub_Type_1__c = 'Assets';
        insert glAccount;

        return glAccount;
    }



    public static List<AcctSeed__GL_Account__c> createGLAccounts(){
        // create GL Accounts
        AcctSeed__GL_Account__c[] glAccounts = new List<AcctSeed__GL_Account__c>();
        // 0
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '1000-Cash',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Bank__c = true,
                        AcctSeed__Sub_Type_1__c = 'Assets',
                        AcctSeed__Sub_Type_2__c = 'Cash')
        );

        // 1
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '2330-Unearned Income',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Sub_Type_1__c = 'Assets',
                        AcctSeed__Sub_Type_2__c = 'Cash'
                )
        );

        // 2
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '1200-Accounts Receivable',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Sub_Type_1__c = 'Assets',
                        AcctSeed__Sub_Type_2__c = 'Current Assets'
                )
        );

        // 3
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '1600-Work In Process',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Sub_Type_1__c = 'Assets',
                        AcctSeed__Sub_Type_2__c = 'Current Assets'
                )
        );

        // 4
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '2000-Accounts Payable',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Sub_Type_1__c = 'Liabilities',
                        AcctSeed__Sub_Type_2__c = 'Current Liabilities'
                )
        );

        // 5
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '2010-Vouchers Payable',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Sub_Type_1__c = 'Liabilities',
                        AcctSeed__Sub_Type_2__c = 'Current Liabilities'
                )
        );

        // 6
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '3050-Retained Earnings',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Sub_Type_1__c = 'Owners Equity',
                        AcctSeed__Sub_Type_2__c = 'Equity'
                )
        );

        // 7
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '3060-Current Year Earnings',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Sub_Type_1__c = 'Owners Equity',
                        AcctSeed__Sub_Type_2__c = 'Equity'
                )
        );

        // 8
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '4000-Product Revenue',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Revenue',
                        AcctSeed__Sub_Type_1__c = 'Product Revenue',
                        AcctSeed__Sub_Type_2__c = 'Product Family 1'
                )
        );

        // 9
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '5010-Inventory Cost Variance',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Expense',
                        AcctSeed__Sub_Type_1__c = 'Cost of Goods Sold',
                        AcctSeed__Sub_Type_2__c = 'Materials'
                )
        );

        // 10
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '5040-Vendor Payment Discounts',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Expense',
                        AcctSeed__Sub_Type_1__c = 'Cost of Goods Sold',
                        AcctSeed__Sub_Type_2__c = 'Materials'
                )
        );

        // 11
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = 'zLabor Clearing',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Expense'
                )
        );

        // 12
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = 'zProject Labor',
                        AcctSeed__Active__c = true,
                        AcctSeed__Type__c = 'Expense'
                )
        );

        // 13
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '4900-Customer Payment Discounts',
                        AcctSeed__Active__c = true,
                        AcctSeed__Bank__c = false,
                        AcctSeed__Type__c = 'Revenue',
                        AcctSeed__Sub_Type_1__c = 'Product Revenue',
                        AcctSeed__Sub_Type_2__c = 'Product Family 1'
                )
        );

        // 14
        glAccounts.add(
                new AcctSeed__GL_Account__c(
                        Name = '1400-Prepaid Expenses',
                        AcctSeed__Active__c = true,
                        AcctSeed__Bank__c = false,
                        AcctSeed__Type__c = 'Balance Sheet',
                        AcctSeed__Sub_Type_1__c = 'Assets',
                        AcctSeed__Sub_Type_2__c = 'Current Assets'
                )
        );

        insert glAccounts;

        return glAccounts;
    }

    public static AcctSeed__Accounting_Variable__c createAccountingVariable(String type)
    {
        AcctSeed__Accounting_Variable__c var = new AcctSeed__Accounting_Variable__c();
        var.AcctSeed__Active__c = true;
        var.AcctSeed__Type__c = 'GL Account Variable ' + type;

        insert var;

        return var;
    }

    public static AcctSeed__Accounting_Variable__c createAccountingVariable()
    {
        AcctSeed__Accounting_Variable__c var = new AcctSeed__Accounting_Variable__c();
        var.AcctSeed__Active__c = true;
        var.AcctSeed__Type__c = 'GL Account Variable 1';

        insert var;

        return var;
    }

    public static AcctSeed__Ledger__c createLedger(Id pricebookId, String ledgerName, Id glAccountId, id bfId)
    {
        AcctSeed__Ledger__c ledger = new AcctSeed__Ledger__c();
        ledger.Name = ledgerName;
        //Please note. Only one Transactional Ledger allowed at a time.
        ledger.AcctSeed__Type__c = 'Transactional';
        ledger.AcctSeed__Billing_Activity_Statement_Format__c = bfid;
        ledger.AcctSeed__Billing_Outstanding_Statement_Format__c = bfid;
        ledger.AcctSeed__Default_Bank_Account__c = glAccountid;
        ledger.AcctSeed__Default_Billing_Format__c = bfid;
        ledger.AcctSeed__Default_Purchase_Order_Format__c = bfid;
        ledger.AcctSeed__Default_Packing_Slip_Format__c = bfid;
        insert ledger;

        return ledger;
    }

    //Needs a Purchase Order format, a Transactional Ledger, a Vendor account
    public static AcctSeedERP__Purchase_Order__c createPurchaseOrder()
    {
        AcctSeedERP__Purchase_Order__c po = new AcctSeedERP__Purchase_Order__c();
        
        //Create the PO Format for the PO
        AcctSeed__Billing_Format__c bf= createPOFormat();
        po.AcctSeedERP__Purchase_Order_Format__c=bf.id;

        //Create the Ledger for the PO. 
        //Note: Cannot manually create another transactional ledger due to AS VR. Have to SOQL for one.
                //AcctSeed__GL_Account__c glAccount= createGLAccount('Test glAccount','Balance Sheet',True);
                //AcctSeed__Ledger__c ledger=createLedger(Test.getStandardPricebookId(),'Test Ledger',glAccount.id,bf.id);
        AcctSeed__Ledger__c ledger=[Select Id FROM AcctSeed__Ledger__c WHERE AcctSeed__Type__c = 'Transactional'];
        po.AcctSeedERP__Ledger__c= ledger.Id;

        //Create the Vendor for the PO
        AcctSeed__Accounting_Variable__c glAccountVar= createAccountingVariable('1');
        Account vendor= createVendor(glAccountVar.id);
        po.AcctSeedERP__Vendor__c=vendor.id;

        po.AcctSeedERP__Status__c='Draft';
            
        return po;
    }
    public static Product2 createProduct(id glAccountR, id glAccountE, id glAccountI)
    {
        Product2 prod = new Product2();
        prod.Name = 'Test Product';
        prod.Family = 'Tax';
        prod.AcctSeed__Revenue_GL_Account__c = glAccountR;
        prod.AcctSeed__Expense_GL_Account__c = glAccountE;
        prod.AcctSeed__Inventory_GL_Account__c = glAccountI;
        prod.AcctSeed__Inventory_Type__c = 'Inventory';
        prod.AcctSeed__Unit_Cost__c = 1;
        prod.Manufacturers_Suggested_Retail_Price__c=5;
        prod.Price__c=10;
        prod.Price_CAN__c=10;
        prod.AcctSeed__Inventory_Type__c='Purchased';
        prod.AcctSeed__Inventory_Product__c=true;
        prod.Catalog_Available_Qty__c=0;
        insert prod;
        return prod;
    }

    public static Product2 createProduct(id glAccountR, id glAccountE, id glAccountI, Boolean isInsert)
    {
        Product2 prod = new Product2();
        prod.Name = 'TEST - Developer Product';
        prod.Family = 'Tax';
        prod.AcctSeed__Revenue_GL_Account__c = glAccountR;
        prod.AcctSeed__Expense_GL_Account__c = glAccountE;
        prod.AcctSeed__Inventory_GL_Account__c = glAccountI;
        prod.AcctSeed__Inventory_Type__c = 'Inventory';
        prod.AcctSeed__Unit_Cost__c = 1;
        prod.Website_IDs__c='4,5';
        prod.Manufacturers_Suggested_Retail_Price__c=5;
        prod.Manufacturer_Suggested_Retail_Price_CAN__c=5;
        prod.Price__c=10;
        prod.Price_CAN__c=10;
        prod.AcctSeed__Inventory_Type__c='Purchased';
        prod.AcctSeed__Inventory_Product__c=true;
        prod.Catalog_Available_Qty__c=0;
        if(isInsert){
           insert prod;
        }
        
        return prod;
    }

    public static Map<String,Product2> createProducts(id glAccount, id expenseGl, id inventoryGL)
    {
        List<Product2> prods = new List<Product2>();

        Product2 prod = new Product2();
        prod.Name = 'State/City Tax';
        prod.Family = 'Tax';
        prod.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod.Manufacturers_Suggested_Retail_Price__c=5;
        prod.Price__c=10;
        prod.Price_CAN__c=10;
        prods.add(prod);

        Product2 prod2 = new Product2();
        prod2.Name = 'State/County Tax';
        prod2.Family = 'Tax';
        prod2.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod2.Manufacturers_Suggested_Retail_Price__c=5;
        prod2.Price__c=10;
        prod2.Price_CAN__c=10;        
        prods.add(prod2);

        Product2 prod3 = new Product2();
        prod3.Name = 'State Tax';
        prod3.Family = 'Tax';
        prod3.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod3.Manufacturers_Suggested_Retail_Price__c=5;
        prod3.Price__c=10;
        prod3.Price_CAN__c=10;        
        prods.add(prod3);

        Product2 prod4 = new Product2();
        prod4.Name = 'Test Maintenance Prod';
        prod4.Family = 'Maintenance';
        prod4.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod4.AcctSeed__Inventory_Product__c = true;
        prod4.AcctSeed__Expense_GL_Account__c = expenseGl;
        prod4.AcctSeed__Inventory_GL_Account__c = inventoryGL;
        prod4.AcctSeed__Inventory_Type__c = 'Inventory';
        prod4.AcctSeed__Unit_Cost__c = 1;
        prod4.Manufacturers_Suggested_Retail_Price__c=5;
        prod4.Price__c=10;   
        prod4.Price_CAN__c=10;     
        prods.add(prod4);

        Product2 prod5 = new Product2();
        prod5.Name = 'Credit Card Fee';
        prod5.Family = 'Convenience Fee';
        prod5.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod5.Manufacturers_Suggested_Retail_Price__c=5;
        prod5.Price__c=10;  
        prod5.Price_CAN__c=10;      
        prods.add(prod5);

        Product2 prod6 = new Product2();
        prod6.Name = 'Residential svc $10';
        prod6.Family = 'Late Fee';
        prod6.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod6.Manufacturers_Suggested_Retail_Price__c=5;
        prod6.Price__c=10;      
        prod6.Price_CAN__c=10;  
        prods.add(prod6);

        Product2 prod7 = new Product2();
        prod7.Name = 'Repairs - Parts & Chems 10%';
        prod7.Family = 'Late Fee';
        prod7.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod7.Manufacturers_Suggested_Retail_Price__c=5;
        prod7.Price__c=10;   
        prod7.Price_CAN__c=10;     
        prods.add(prod7);

        Product2 prod8 = new Product2();
        prod8.Name = 'Commercial Service < $200';
        prod8.Family = 'Late Fee';
        prod8.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod8.Manufacturers_Suggested_Retail_Price__c=5;
        prod8.Price__c=10;    
        prod8.Price_CAN__c=10;
        prods.add(prod8);

        Product2 prod9 = new Product2();
        prod9.Name = 'Commercial Service >= $200';
        prod9.Family = 'Late Fee';
        prod9.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod9.Manufacturers_Suggested_Retail_Price__c=5;
        prod9.Price__c=10;  
        prod9.Price_CAN__c=10;      
        prods.add(prod9);

        Product2 prod10 = new Product2();
        prod10.Name = 'Remodels - 1.5%/Month';
        prod10.Family = 'Late Fee';
        prod10.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod10.Manufacturers_Suggested_Retail_Price__c=5;
        prod10.Price__c=10; 
        prod10.Price_CAN__c=10;       
        prods.add(prod10);

        Product2 prod11 = new Product2();
        prod11.Name = 'Test Part';
        prod11.Family = 'Parts';
        prod11.AcctSeed__Revenue_GL_Account__c = glAccount;
        prod11.AcctSeed__Expense_GL_Account__c = expenseGl;
        prod11.AcctSeed__Inventory_GL_Account__c = inventoryGL;
        prod11.AcctSeed__Inventory_Type__c = 'Purchased';
        prod11.AcctSeed__Inventory_Product__c = true;
        prod11.AcctSeed__Unit_Cost__c = 1;
        prod11.Manufacturers_Suggested_Retail_Price__c=5;
        prod11.Price__c=10;  
        prod11.Price_CAN__c=10;      
        prods.add(prod11);

        insert prods;

        Map<String,Product2> prodMap = new Map<String,Product2>();
        for(Product2 p:prods)
        {
            prodMap.put(p.Name,p);
        }

        return prodMap;
    }

    public static AcctSeedERP__Purchase_Order_Line__c createPOLine(String po, String prod, Decimal quantity, String glav2, String glav3)
    {
        AcctSeedERP__Purchase_Order_Line__c pol= new AcctSeedERP__Purchase_Order_Line__c();
        pol.AcctSeedERP__Purchase_Order__c=po;
        pol.AcctSeedERP__Product__c=prod;
        pol.AcctSeedERP__Quantity__c=quantity;
        pol.AcctSeedERP__GL_Account_Variable_2__c=glav2;
        pol.AcctSeedERP__GL_Account_Variable_3__c=glav3;
        pol.AcctSeedERP__Unit_Price__c=5;

        //insert pol;
        return pol;

    }

    public static Opportunity createOpportunity()
    {
        Opportunity opp = new Opportunity();
        opp.Name='Test Opportunity';
        opp.StageName='Open';
        opp.CloseDate=Date.today();
        insert opp;
        return opp;
    }

    public static npe01__OppPayment__c createPayment()
    {
        npe01__OppPayment__c pmt= new npe01__OppPayment__c();
        Opportunity opp=createOpportunity();
        pmt.npe01__Opportunity__c= opp.Id;

        return pmt;
    }

    public static List<Supplier_Discount__c> createVendorDiscounts(Account a,Integer length)
    {
        List<Supplier_Discount__c> venDiscs= new List<Supplier_Discount__c>();
        for (Integer i = 0; i < length; i++) {
                Supplier_Discount__c vd = new Supplier_Discount__c();
                vd.Vendor__c=a.Id;
                vd.Discount_Terms__c='Single Title';
                vd.Min_qty__c=0;
                vd.Max_Qty__c=2;
                vd.Discount__c=50;
                venDiscs.add(vd);
        }

        insert venDiscs;
        return venDiscs;

    }
    public static AcctSeedERP__Warehouse__c createWarehouse()
    {
        AcctSeedERP__Warehouse__c wh= new AcctSeedERP__Warehouse__c();

        wh.Name='TEST - Steve Warehouse';

        insert wh;
        return wh;

    }

    public static AcctSeedERP__Location__c createLocation(String warehouseId)
    {
        AcctSeedERP__Location__c loc= new AcctSeedERP__Location__c();
        loc.Name='Test Location';
        loc.AcctSeedERP__Warehouse__c=warehouseId;

        insert loc;
        return loc;

    }

    public static AcctSeedERP__Inventory_Balance__c createIQA(Product2 prod)
    {
        AcctSeedERP__Inventory_Balance__c iqa= new AcctSeedERP__Inventory_Balance__c();
        iqa.AcctSeedERP__Product__c=prod.id;
        AcctSeed__Ledger__c ledger=[Select Id FROM AcctSeed__Ledger__c WHERE AcctSeed__Type__c = 'Transactional'];
        iqa.AcctSeedERP__Ledger__c=ledger.id;
        AcctSeedERP__Warehouse__c wh= createWarehouse();
        iqa.AcctSeedERP__Warehouse__c=wh.id;
        iqa.AcctSeedERP__Location__c=createLocation(wh.id).id;

        insert iqa;
        return iqa;
    }

    @isTest
    public static void testUtilsTest(){

                // Create the PO
                AcctSeedERP__Purchase_Order__c po = createPurchaseOrder();
                insert po;

                // Create the Product
                AcctSeed__GL_Account__c glAccountR= createGLAccount('Test glAccount','Revenue',True);
                AcctSeed__GL_Account__c glAccountE= createGLAccount('Test glAccount','Expense',True);
                AcctSeed__GL_Account__c glAccountI= createGLAccount('Test glAccount','Balance Sheet',True);
                Product2 prod= createProduct(glAccountR.id,glAccountE.id,glAccountI.id);

                List<AcctSeedERP__Purchase_Order_Line__c> polList = new List<AcctSeedERP__Purchase_Order_Line__c>();
                // Create the PO Line(s)
                String glav2=createAccountingVariable('2').id;
                String glav3=createAccountingVariable('3').id;
                String poid= po.id;
                String prodid= prod.id;
                for (Integer i = 0; i < 250; i++) {
                        AcctSeedERP__Purchase_Order_Line__c pol=createPOLine(poid,prodid,5,glav2,glav3);
                        pol.Warehouse__c='a402f0000000NUqAAM';
                        pol.Location__c='a3o2f0000004CymAAE';
                        polList.add(pol);  
                }
                insert polList;

                //Update the POLs
                /*
                for (AcctSeedERP__Purchase_Order_Line__c pol:polList) {
                        pol.AcctSeedERP__Unit_Price__c=2;
                }
                update polList;
                
                po.Receiving_Warehouse__c='a402f0000000NUqAAM';
               // if virtual warehouse:
               // po.Receiving_Warehouse__c='a403I0000004YDEQA2';
                po.AcctSeedERP__Status__c='Sent';
                update po;

                System.assertEquals('Draft', po.AcctSeedERP__Status__c);*/

                delete po;


                


    }



}