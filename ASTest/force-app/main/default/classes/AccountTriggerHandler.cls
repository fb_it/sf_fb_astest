/*
Created by Steve Reitz, on February 27, 2020. Sreitz@firstbook.org

*/

public without sharing class AccountTriggerHandler {


    public static void createGLAV(String accountName ) {
        AcctSeed__Accounting_Variable__c AVList = new AcctSeed__Accounting_Variable__c(Name=accountName,acctSeed__Type__c='GL Account Variable 3');
        insert AVList;
    }
    /*
    public static void clearTLP(Account a){
        a.Top_Level_Parent__c=null;
    }*/

    public static void changePOLevel(Account aNew, Boolean isUncheck){
        System.debug(aNew.id);
        String flowName='Account_Create_and_Edit_POs_at_non_TLP_level';
	    Map<String, Object> inputs = new Map<String, Object>();
        inputs.put('recordId', aNew);
        inputs.put('Account ID', aNew.id);
        Flow.Interview myFlow = Flow.Interview.createInterview(flowName, inputs);
        myFlow.start();
        if(isUncheck){
            List<Supplier_Discount__c> suppDiscToDel = new List<Supplier_Discount__c>();
            suppDiscToDel= [SELECT Id FROM Supplier_Discount__c WHERE Vendor__c=:aNew.Id];
            delete suppDiscToDel;
        }
    }

}