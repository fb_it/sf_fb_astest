public with sharing class InboundInventoryMovTriggerHandler {

    @future(Callout=true)
    public static void SendToMagento(String newListJSON, Boolean isDelete)
    {

        List<AcctSeedERP__Inbound_Inventory_Movement__c> newList = (List<AcctSeedERP__Inbound_Inventory_Movement__c>)JSON.deserialize(newListJSON,List<AcctSeedERP__Inbound_Inventory_Movement__c>.class);
        String q ='Select id, Username__c, API_Key__c from Magento_Setting__mdt ';
        if(runningInASandbox)
        {
            q += 'WHERE DeveloperName = \'Sandbox\' limit 1';
        }
        else
        {
            q += 'WHERE DeveloperName = \'Production\' limit 1';
        }

        Magento_Setting__mdt magentoSetting = Database.query(q);
        magento2.Port m = new magento2.Port();
        String sessionId = 'testtoken';
        if(!test.isRunningTest())
        {
            sessionId = m.login(magentoSetting.Username__c,magentoSetting.API_Key__c);
        }

        system.debug(LoggingLevel.Error,'sessionId is '+sessionId);

        for(AcctSeedERP__Inbound_Inventory_Movement__c iim:[Select id, AcctSeedERP__Warehouse__c, AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c, AcctSeedERP__Quantity__c, AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Location__r.Magento_Sync__c from AcctSeedERP__Inbound_Inventory_Movement__c where ID in: newList AND AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Location__r.Magento_Sync__c = TRUE AND AcctSeedERP__Quantity__c >0 ALL ROWS])
        {
            Integer quantity = isDelete ? (Integer)iim.AcctSeedERP__Quantity__c *-1 : (Integer)iim.AcctSeedERP__Quantity__c;
            String warehouse;
            if(iim.AcctSeedERP__Warehouse__c == 'Maple Logistics')
            {
                warehouse = '10';
            }
            else if(iim.AcctSeedERP__Warehouse__c == 'American Book Company')
            {
                warehouse = '8';
            }
            system.debug('Quantity -- ' + String.valueof(quantity));
            m.catalogInventoryStockItemUpdateDelta(sessionId,iim.AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c,(Integer)quantity);
            m.catalogInventoryStockItemUpdateErp(sessionId,warehouse,iim.AcctSeedERP__Inventory_Balance__r.AcctSeedERP__Product__r.tnw_mage_basic__Magento_ID__c,String.valueof(quantity));
        }


    }

    public static Boolean runningInASandbox {
        get {
            if (runningInASandbox == null) {
                runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
            }
            return runningInASandbox;
        }
        set;
    }

}