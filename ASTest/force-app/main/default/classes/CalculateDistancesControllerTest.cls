@isTest public class CalculateDistancesControllerTest {

    @istest public static void calculateDistancesTest(){
        CalculateDistancesController cntrl = new CalculateDistancesController();
        
        Test.setCurrentPageReference(new PageReference('Page.CalculateDistances')); 
        
        Test.startTest();
        
        Account testAcc = new Account (Name = 'Test Account');
        testAcc.BillingStreet = '3275 NW 24th Street Rd';
        testAcc.BillingCity = 'Miami';
        testAcc.BillingCountry = 'US';
        testAcc.BillingState = 'FL';
        testAcc.BillingLatitude = 25.7617;
        testAcc.BillingLongitude = -80.1918;
        
        insert testAcc;

        //Creates Contact to be linked to Campaign Member
        Contact testContact = new Contact(FirstName = 'TestContactF', LastName = 'TestContactL', Email = 'none@none.net',accountid=testAcc.Id);
        //testContact.
        testContact.MailingLatitude = 25.7617;
        testContact.MailingLongitude = -80.1918;
        testContact.Magento_Organization__c = testAcc.id;
        
        insert testContact;
        
        Campaign camp = new Campaign(
                            Name = 'Test',
                            IsActive = TRUE
                            );            
        insert camp;

        testAcc.Current_POI_Campaign__c = camp.Id;
        update testAcc;
        
        //Creates a new campaign memeber, associaites it with 1 provider sales campaign, and inserts
        CampaignMember newMember = 
        new CampaignMember(ContactId = testContact.id, status='Sent', campaignid = camp.id);
        insert newMember;                 
        
        String campaignId = camp.id;
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('campaignId',campaignId);
		cntrl.calculateDistances(); 
        
        Test.setCurrentPageReference(new PageReference('Page.DeleteDistances')); 
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('campaignId',campaignId);
		cntrl.deleteExistingDistances();
        
        Test.stopTest();
    }
    
    @istest public static void deleteExistingDistancesTest(){  
        
        Test.startTest();

        Account testAcc = new Account (Name = 'Test Account');
        testAcc.BillingStreet = '3275 NW 24th Street Rd';
        testAcc.BillingCity = 'Miami';
        testAcc.BillingCountry = 'US';
        testAcc.BillingState = 'FL';
        testAcc.BillingLatitude = 25.7617;
        testAcc.BillingLongitude = -80.1918;
        
        insert testAcc;

        //Creates Contact to be linked to Campaign Member
        Contact testContact = new Contact(FirstName = 'TestContactF', LastName = 'TestContactL', Email = 'none@none.net',accountid=testAcc.Id);
        //testContact.
        testContact.MailingLatitude = 25.7617;
        testContact.MailingLongitude = -80.1918;
        
        insert testContact;
        
        Campaign camp = new Campaign(
                            Name = 'Test',
                            IsActive = TRUE
                            );            
        insert camp;

        testAcc.Current_POI_Campaign__c = camp.Id;
        update testAcc;
        
        //Creates a new campaign memeber, associaites it with 1 provider sales campaign, and inserts
        CampaignMember newMember = 
        new CampaignMember(ContactId = testContact.id, status='Sent', campaignid = camp.id);
        insert newMember;                 

        Calculated_Distance__c calDist = new Calculated_Distance__c();
        calDist.Account__c = testAcc.Id;
        calDist.Contact__c = testContact.Id;
        calDist.Campaign__c = camp.Id;

        insert calDist;
        
        DeleteCalculatedDistances calDis = new DeleteCalculatedDistances(camp.id, 'delete');
		Database.executeBatch(calDis, 9000);
        
		Test.stopTest();        
    } 
 
}