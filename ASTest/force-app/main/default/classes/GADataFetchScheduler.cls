global with sharing class GADataFetchScheduler implements Database.Batchable<SObject>,Database.AllowsCallouts {

    String source;    
    
    global GADataFetchScheduler(String callSource){
    	source = callSource;    
    }
    
    global Iterable<sObject> start(Database.BatchableContext BC){
       	GADataFetch mkd = new GADataFetch();
        System.debug('GADataFetchScheduler::start');
       	return mkd.fetchData(null,null);
    }

    global void execute(Database.BatchableContext BC, list<Opportunity> opportunityList){
        System.debug('GADataFetchScheduler::execute');
        //if(!opportunityList.isEmpty()){
            try{
                Database.update(opportunityList, false);
                System.debug('After OpportunityList Update.');
            }catch (DMLException e){
                System.debug('Exception:'+e.getMessage());
            }            
        //}        
    }
    
    // Create a new schedule using System.schedule for the GADataFetch class
    global void finish(Database.BatchableContext BC){    
        if(source == 'Batch'){
            Datetime dt = System.now();
            String schedule='0 0 0/8 * * ?';
            //String schedule = '0 0 * * * ?';
            String name = 'GADataFetch'; 
            
            GADataFetchJob nextBatchJob = new GADataFetchJob();
            System.schedule(name,schedule, nextBatchJob);            
        }      
        /*
      System.schedule('GADataFetch1','0 0 * * * ?', nextBatchJob);
      System.schedule('GADataFetch2','0 15 * * * ?', nextBatchJob);
      System.schedule('GADataFetch3','0 30 * * * ?', nextBatchJob);
      System.schedule('GADataFetch4','0 45 * * * ?', nextBatchJob);
	*/	    
    } 
}