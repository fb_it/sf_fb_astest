global without sharing class CatalogProductUpdateActions implements Queueable {

    global List<CatalogProductUpdateRequest> productVals;

    // global List<Product2> productVals;


    public CatalogProductUpdateActions(List<CatalogProductUpdateRequest> productVals) {
        this.productVals=productVals;
    }

    public void execute (QueueableContext context){
            if(!Test.isRunningTest()){
                    Database.executeBatch(new CatalogProductUpdateBatchable(productVals), 5);
             }
    }

    @InvocableMethod(label='Catalog Product Update' description='Sends Price, MSRP, and Store to Magento.')
    public static void catalogProductUpdate(List<CatalogProductUpdateRequest> productVals)
    {
      	System.enqueueJob(new CatalogProductUpdateActions(productVals));
    }

    global class CatalogProductUpdateRequest {

        @InvocableVariable(label='Is this a Price and MSRP Update?' required=true)
        global Boolean sendPriceAndMSRP;
        @InvocableVariable(label='Is this a manage_stock update?' required=true)
        global Boolean sendManageStock;

        @InvocableVariable(label='Product Id' required=true)
        global String ProductId;

        @InvocableVariable(label='Website Ids' required=true)
        global String Website_IDs_c;
        @InvocableVariable(label='Magento Id' required=true)
        global String tnw_mage_basic_Magento_ID_c;

        @InvocableVariable(label='Use Config Manage Stock' required=false)
        global Boolean Use_Config_Manage_Stock_c;
        @InvocableVariable(label='Manage Stock' required=false)
        global String Manage_Stock_c;

        @InvocableVariable(label='US Price' required=false)
        global Decimal Price_c;
        @InvocableVariable(label='US MSRP' required=false)
        global Decimal Manufacturers_Suggested_Retail_Price_c;

        @InvocableVariable(label='Canada Price' required=false)
        global Decimal Price_CAN_c;
        @InvocableVariable(label='Canada MSRP' required=false)
        global Decimal Manufacturer_Suggested_Retail_Price_CAN_c;


    }

        // @InvocableMethod(label='Catalog Product2 Update' description='Sends Price, MSRP, and Store to Magento.')
    // public static void catalogProductUpdate(List<Product2> productVals)
    // {

    //   	System.enqueueJob(new CatalogProductUpdateActions(productVals));
    // }

        // public CatalogProductUpdateActions(List<Product2> productVals) {
    //     this.productVals=productVals;
    // }

}