@isTest
global class MagentoWebServiceMockImpl implements WebServiceMock {

    public String type;

    global MagentoWebServiceMockImpl(final String type){
        this.type = type;
    }

  
    global void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
        if(type=='login'){
            magentoSOAP.loginParam_element loginParamElement= 
                new magentoSOAP.loginParam_element();
            magentoSOAP.loginResponseParam_element loginElement= 
                new magentoSOAP.loginResponseParam_element();
            loginElement.result='loggedin';
            response.put('response_x', loginElement);
        }
        if(type=='catalogInventoryStockItemUpdateDelta') {
            magentoSOAP.catalogInventoryStockItemUpdateDeltaResponseParam_element respElement =
                    new magentoSOAP.catalogInventoryStockItemUpdateDeltaResponseParam_element();
            respElement.result = 1;
            response.put('response_x', respElement);
        }
        if(type=='catalogInventoryStockItemUpdateErp') {
            magentoSOAP.catalogInventoryStockItemUpdateErpResponseParam_element respElement =
                    new magentoSOAP.catalogInventoryStockItemUpdateErpResponseParam_element();
            respElement.result = 1;
            response.put('response_x', respElement);
        }
        if(type=='catalogProductUpdate') {
            magentoSOAP.catalogProductUpdateResponseParam_element respElement =
                    new magentoSOAP.catalogProductUpdateResponseParam_element();
            respElement.result = true;
            response.put('response_x', respElement);
        }
        if(type=='catalogProductUpdateFail') {
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Read timed out: Unauthorized endpoint, please check Setup->Security->Remote site settings.');
            throw e;
        }
        if(type=='msrp'){
            magentoSOAP.catalogProductSetSingleDataResponseParam_element respElement =
                new magentoSOAP.catalogProductSetSingleDataResponseParam_element();
            respElement.result=1;
            response.put('response_x', respElement);
        }
/*
        if(type=='salesOrderShipmentCreate')
        {
            magentoSOAP.salesOrderShipmentCreateResponseParam_element respElement =
                    new magentoSOAP.salesOrderShipmentCreateResponseParam_element();
            respElement.result = 'true';
            response.put('response_x', respElement);
        }

        if(type=='salesOrderHold')
        {
            magentoSOAP.salesOrderHoldResponseParam_element respElement =
                    new magentoSOAP.salesOrderHoldResponseParam_element();
            respElement.result = 1;
            response.put('response_x', respElement);
        }

        if(type=='salesOrderUnhold')
        {
            magentoSOAP.salesOrderUnholdResponseParam_element respElement =
                    new magentoSOAP.salesOrderUnholdResponseParam_element();
            respElement.result = 1;
            response.put('response_x', respElement);
        }

        if(type=='salesOrderCancel')
        {
            magentoSOAP.salesOrderCancelResponseParam_element respElement =
                    new magentoSOAP.salesOrderCancelResponseParam_element();
            respElement.result = 1;
            response.put('response_x', respElement);
        }

        if(type=='salesOrderAddComment')
        {
            magentoSOAP.salesOrderAddCommentResponseParam_element respElement =
                    new magentoSOAP.salesOrderAddCommentResponseParam_element();
            respElement.result = 1;
            response.put('response_x', respElement);
        }

        if(type=='salesOrderInvoiceCreate')
        {
            magentoSOAP.salesOrderInvoiceCreateResponseParam_element respElement =
                    new magentoSOAP.salesOrderInvoiceCreateResponseParam_element();
            respElement.result = 'true';
            response.put('response_x', respElement);
        }
*/

    }
}