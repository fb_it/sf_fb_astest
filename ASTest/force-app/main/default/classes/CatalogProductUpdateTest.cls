@isTest
private class CatalogProductUpdateTest {

    @isTest
    private static void testCatalogProductUpdate(){
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('login'));
        
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdate'));
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('msrpFail'));
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdateFail'));

        List<CatalogProductUpdateActions.CatalogProductUpdateRequest> productDML=new List<CatalogProductUpdateActions.CatalogProductUpdateRequest>();
        // AcctSeed__GL_Account__c glAccountR= TestUtils.createGLAccount('Test glAccount','Revenue',True);
        // AcctSeed__GL_Account__c glAccountE= TestUtils.createGLAccount('Test glAccount','Expense',True);
        // AcctSeed__GL_Account__c glAccountI= TestUtils.createGLAccount('Test glAccount','Balance Sheet',True);
        
        for(Integer i=0; i<3;i++){
            CatalogProductUpdateActions.CatalogProductUpdateRequest prod= new CatalogProductUpdateActions.CatalogProductUpdateRequest();
            prod.sendPriceAndMSRP=TRUE;
            prod.sendManageStock=TRUE;
            prod.ProductId=TestUtils.generateId(Product2.sObjectType);
            prod.Website_IDs_c='1,4,5';
            prod.tnw_mage_basic_Magento_ID_c='3288'+i;
            prod.Use_Config_Manage_Stock_c=TRUE;
            prod.Manage_Stock_c='Yes';
            prod.Price_c=5;
            prod.Manufacturers_Suggested_Retail_Price_c=5;
            if(prod.Website_IDs_c.contains('5')){
                prod.Price_CAN_c=6;
                prod.Manufacturer_Suggested_Retail_Price_CAN_c=6;
            }
            

            productDML.add(prod);
        }

        //insert productDML;

        // for (Product2 prod : productDML) {
        //     prod.Price__c=prod.Price__c+1;
        // }
        // update productDML;
        
        
        // List<CatalogProductUpdateActions.CatalogProductUpdateRequest> prodList= new List<CatalogProductUpdateActions.CatalogProductUpdateRequest>();
        // prodList.add(prod);
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('login'));
        
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdate'));
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('msrpFail'));
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdateFail'));
      
        CatalogProductUpdateActions queueable= new CatalogProductUpdateActions(productDML);
        CatalogProductUpdateActions.catalogProductUpdate(productDML);
        Database.executeBatch(new CatalogProductUpdateBatchable(productDML), 3);



        
        Test.stopTest();
        
        //System.assertEquals(1,prodList.size());
        
        //CatalogProductUpdateActions.catalogProductUpdateBatch(new List<CatalogProductUpdateActions.CatalogProductUpdateRequest>{prod});
    }
}