global without sharing class CalculateDistances implements Database.Batchable<SObject>{
    
    private String campId;
    private Integer numAccounts;
    private Decimal maxDistance;
    
    private List<Calculated_Distance__c> calculatedDistanceList = new List<Calculated_Distance__c>();
    
    global CalculateDistances(String cmpId, Integer numAccts, Decimal distance){
    	campId = cmpId;
        numAccounts = numAccts;
        maxDistance = distance;
    }    

    global Database.QueryLocator start(Database.BatchableContext BC) {  
        System.debug('CalculateDistances: starting batch for campId:' + campId);
                
        String query = 'select CampaignId, contactId, City, State, Street, PostalCode, Contact.FirstName, ' +
            			'Contact.LastName, Contact.Magento_Organization__c, Contact.OtherAddress, Contact.OtherPhone, ' + 
            			'Contact.npe01__HomeEmail__c, Contact.Member__c, Contact.Address_Latitude__c, Contact.Address_Longitude__c, ' +
            			'Contact.Magento_Organization__r.Address_Latitude__c, Contact.Magento_Organization__r.Address_Longitude__c, ' +
            			'Contact.Red_Lab_Tags_Formula__c, Contact.Last_Magento_Completed_Order__c, Contact.Magento_Organization__r.Name, ' + 
            			'Contact.Magento_Organization__r.BillingAddress ' +
            			'from CampaignMember where campaignId = :campId ' + 
            			'and Contact.Magento_Organization__r.Address_Latitude__c != 0 and Contact.Magento_Organization__r.Address_Longitude__c != 0';

        System.debug('CalculateDistances: start query: ' + query);
        
        return Database.getQueryLocator(query);
	} 
    
    global void execute(Database.BatchableContext BC, List<CampaignMember> campaignMembers){
        String queryString;
        List<Account> accList;
        calculatedDistanceList = new List<Calculated_Distance__c>();
        
        for(CampaignMember cmpMember : campaignMembers){
            
            queryString =
                'SELECT Name, Id, DISTANCE(BillingAddress, GEOLOCATION('+ cmpMember.Contact.Magento_Organization__r.Address_Latitude__c + ',' + cmpMember.Contact.Magento_Organization__r.Address_Longitude__c + '), \'mi\') ' +
                'FROM Account ' +
                'WHERE DISTANCE(BillingAddress, GEOLOCATION('+ cmpMember.Contact.Magento_Organization__r.Address_Latitude__c + ',' + cmpMember.Contact.Magento_Organization__r.Address_Longitude__c + '), \'mi\') < :maxDistance ' +
                //'AND RecordType.Name=\'Organization\' ' +
                'AND Current_POI_Campaign__c = :campId and BillingStreet != null ' +
                'ORDER BY DISTANCE(BillingAddress, GEOLOCATION('+ cmpMember.Contact.Magento_Organization__r.Address_Latitude__c + ',' + cmpMember.Contact.Magento_Organization__r.Address_Longitude__c + '), \'mi\') ' +            
                'LIMIT :numAccounts';
            
            System.debug('CalculateDistances:execute:queryString:' + queryString);
            
            accList = Database.query(queryString);

            for(Account acct : accList){
                
                System.debug('CalculateDistances:execute:acct:' + acct);
                
            	Calculated_Distance__c calDist = new Calculated_Distance__c();
                calDist.Account__c = acct.Id;
                calDist.Contact__c = cmpMember.ContactId;
                calDist.Campaign__c = cmpMember.CampaignId;
                calDist.Email__c = cmpMember.Contact.npe01__HomeEmail__c;
                calDist.Phone__c = cmpMember.Contact.OtherPhone;
                calDist.Magento_Organization__c = cmpMember.Contact.Magento_Organization__r.Name;
                calDist.FirstName__c = cmpMember.Contact.FirstName;
                calDist.LastName__c = cmpMember.Contact.LastName;
                calDist.IsMember__c = cmpMember.Contact.Member__c;
                calDist.Distance__c = (Decimal)(acct.get('expr0'));
                calDist.City__c = cmpMember.City;
                calDist.State__c = cmpMember.State;
                calDist.Street_Address__c = cmpMember.Street;
                calDist.Zip__c = cmpMember.PostalCode;
                calDist.Organization_Type__c = cmpMember.Contact.Red_Lab_Tags_Formula__c;
                calDist.Date_of_Last_Magento_Order__c = cmpMember.Contact.Last_Magento_Completed_Order__c;
                
                calculatedDistanceList.add(calDist);
                
                System.debug('CalculateDistances:execute:calDist:' + calDist);
            }            
        }

        if(!calculatedDistanceList.isEmpty()){
            Database.insert(calculatedDistanceList, false);            
        }        
    }
    
    global void finish(Database.BatchableContext BC){    
   		CalculateDistancesController cntrl = new CalculateDistancesController();
        cntrl.distanceCalculationComplete(campId);    	    
    }    
}