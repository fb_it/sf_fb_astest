global class CatalogProductUpdateBatchable implements Database.Batchable<Product2>,Database.AllowsCallouts,Database.Stateful {
    

    global List<Product2> product2UpdateBatch;
    global List<Product2> failedProduct2Syncs;
    public String store;
    public Decimal msrp;
    public Decimal price;
    global Boolean sendPriceAndMSRP=FALSE;
    global Boolean sendManageStock=FALSE;

        
    public CatalogProductUpdateBatchable(List<Product2> product2UpdateBatch) {
        this.product2UpdateBatch=product2UpdateBatch;
    }

    public CatalogProductUpdateBatchable(List<CatalogProductUpdateActions.CatalogProductUpdateRequest> queuedProds) {
        List<Product2> mockedProducts = new List<Product2>();
        for (CatalogProductUpdateActions.CatalogProductUpdateRequest requestProd : queuedProds) {
            Product2 batchableProd= new Product2();
            batchableProd.id=requestProd.ProductId;
            batchableProd.Website_IDs__c=requestProd.Website_IDs_c;
            batchableProd.tnw_mage_basic__Magento_ID__c=requestProd.tnw_mage_basic_Magento_ID_c;

            if (requestProd.sendManageStock==TRUE) {
                this.sendManageStock=TRUE;
                batchableProd.Use_Config_Manage_Stock__c=requestProd.Use_Config_Manage_Stock_c;
                batchableProd.Manage_Stock__c=requestProd.Manage_Stock_c;
            }
            if (requestProd.sendPriceAndMSRP==TRUE) {
                this.sendPriceAndMSRP=TRUE;
                batchableProd.Price__c=requestProd.Price_c;
                batchableProd.Manufacturers_Suggested_Retail_Price__c=requestProd.Manufacturers_Suggested_Retail_Price_c;
                if(batchableProd.Website_IDs__c.contains('5')){
                    batchableProd.Price_CAN__c=requestProd.Price_CAN_c;
                    batchableProd.Manufacturer_Suggested_Retail_Price_CAN__c=requestProd.Manufacturer_Suggested_Retail_Price_CAN_c;
                }
            }

            
            mockedProducts.add(batchableProd);
        }

       this.product2UpdateBatch=mockedProducts;
    }

    global Iterable<Product2> start(Database.BatchableContext context) {
        return product2UpdateBatch;
    }
    global void execute(Database.BatchableContext context, List<Product2> product2UpdateBatch) {
        
        magentoSOAP.Port m = new magentoSOAP.Port();
        String sessionId;
        try{
            sessionId = MagentoCalls.fetchSessionID();
        }
        catch(CalloutException ce){
            if (ce.getMessage().contains('Read timed out')) {
                for (Product2 prod : product2UpdateBatch) {
                    prod.addError('URGENT ERROR: Check on the status of Magento. ');
                    
                }
            }
            
        }
        magentoSOAP.catalogInventoryStockItemUpdateEntity stock_data = new magentoSOAP.catalogInventoryStockItemUpdateEntity();
        magentoSOAP.catalogProductCreateEntity productData = new magentoSOAP.catalogProductCreateEntity();
        List<Product2> retryProducts=new List<Product2>();
        List<Product2> failedProduct2Syncs=new List<Product2>();
        System.debug('sendManageStock '+' sendPriceAndMSRP '+sendPriceAndMSRP);
        if(sendManageStock!=null||sendPriceAndMSRP!=null){
            for(Product2 prod: product2UpdateBatch){  

                String productId = prod.tnw_mage_basic__Magento_ID__c;
                System.debug(productId);
                String identifierType = 'id';

                if(sendManageStock){
                    if(prod.Manage_Stock__c=='Yes'){
                        stock_data.manage_stock=1;
                    }
                    else{
                        stock_data.manage_stock=0;
                    }

                    if(prod.Use_Config_Manage_Stock__c==TRUE){
                        stock_data.use_config_manage_stock=1;
                    }
                    else{
                        stock_data.use_config_manage_stock=0;
                    }

                    productData.stock_data= stock_data;
                }

                if (prod.Website_IDs__c.contains('5')) {
                    store='firstbook_can';

                    if(sendPriceAndMSRP){
                        price=prod.Price_CAN__c;
                        System.debug(price);
                        productData.price = String.valueof(price.setScale(2));
                    }
                    try{
                        if(Test.isRunningTest()){
                            Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdate'));
                        }
                        Boolean productUpdateResponse = m.catalogProductUpdate(sessionId,productId,productData,store,identifierType);
                        if(sendPriceAndMSRP){
                            msrp=prod.Manufacturer_Suggested_Retail_Price_CAN__c;        
                            String msrpString= String.valueof(msrp.setScale(2));
                            if(Test.isRunningTest()){
                                Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('msrp'));
                            }
                            Integer msrpResponse = m.catalogProductSetSingleData(sessionId,productId,'msrp',msrpString,store);
                        }
                    }
                    catch(CalloutException ce){
                        CalloutException e = new CalloutException();
                        e.setMessage(productId+' '+ce);
                        if (ce.getMessage().contains('Read timed out')) {
                            retryProducts.add(prod);                           
                        }
                    }
                } //end Canada

                //always send to US
                store='firstbook_us';

                if(sendPriceAndMSRP){
                    price=prod.Price__c;
                    productData.price = String.valueof(price.setScale(2));
                }

                try{
                    if(Test.isRunningTest()){
                        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogProductUpdateFail'));
                    }
                    Boolean productUpdateResponse = m.catalogProductUpdate(sessionId,productId,productData,store,identifierType);
                    if(sendPriceAndMSRP){
                        msrp=prod.Manufacturers_Suggested_Retail_Price__c;        
                        String msrpString= String.valueof(msrp.setScale(2));
                        if(Test.isRunningTest()){
                            Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('msrp'));
                        }
                        Integer msrpResponse = m.catalogProductSetSingleData(sessionId,productId,'msrp',msrpString,store);
                    }
                }
                catch(CalloutException ce){
                    CalloutException e = new CalloutException();
                    e.setMessage(productId+' '+ce);
                    if (ce.getMessage().contains('Read timed out')) {
                        retryProducts.add(prod);                           
                    }
                    else{
                        failedProduct2Syncs.add(prod);
                    }
                }
            } //end for Product loop
        }  //end  if (either)

        if (retryProducts.size()>0) {
            List<CatalogProductUpdateActions.CatalogProductUpdateRequest> mockedRetryProds= new List<CatalogProductUpdateActions.CatalogProductUpdateRequest>();
            for (Product2 retryProd: retryProducts) {
                CatalogProductUpdateActions.CatalogProductUpdateRequest mockedProd = new CatalogProductUpdateActions.CatalogProductUpdateRequest();
                mockedProd.sendPriceAndMSRP=TRUE;
                mockedProd.sendManageStock=FALSE;
                mockedProd.ProductId=retryProd.Id;
                mockedProd.Website_IDs_c=retryProd.Website_IDs__c;
                mockedProd.tnw_mage_basic_Magento_ID_c=retryProd.tnw_mage_basic__Magento_ID__c;
                if(sendPriceAndMSRP){
                    mockedProd.Price_c=retryProd.Price__c;
                    mockedProd.Manufacturers_Suggested_Retail_Price_c=retryProd.Manufacturers_Suggested_Retail_Price__c;
                    if (mockedProd.Website_IDs_c.contains('5')) {
                        mockedProd.Price_CAN_c=retryProd.Price_CAN__c;
                        mockedProd.Manufacturer_Suggested_Retail_Price_CAN_c=retryProd.Manufacturer_Suggested_Retail_Price_CAN__c;
                    }
                }
                if(sendManageStock){
                    mockedProd.Use_Config_Manage_Stock_c=retryProd.Use_Config_Manage_Stock__c;
                    mockedProd.Manage_Stock_c=retryProd.Manage_Stock__c;
                }
                mockedRetryProds.add(mockedProd);
            }
            System.enqueueJob(new CatalogProductUpdateActions(mockedRetryProds));
        }
        if (failedProduct2Syncs.size()>0) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'sreitz@firstbook.org'};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Magento Update Batch Failed for Product(s) ');
            mail.setPlainTextBody('The following Products failed to sync to Magento: '+failedProduct2Syncs.toString());
            // Messaging.sendMassEmail();    
            // Messaging.sendEmailMessage(new Messaging.SingleEmailMessage[] { mail });
            // Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail })           
                
        }
    }
    global void finish(Database.BatchableContext context) {
    }
}