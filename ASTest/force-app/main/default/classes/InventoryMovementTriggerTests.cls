@IsTest(seeAllData=true)
private class InventoryMovementTriggerTests {

    @IsTest(seeAllData=true)
    static void testPOIMTriggerHandler_isInsert() {
   
        
        Test.setMock(WebServiceMock.class, new MagentoWebServiceMockImpl('catalogInventoryStockItemUpdateDelta'));
       
        AcctSeed__GL_Account__c glAccount = TestUtils.createGLAccount('test gl', 'Revenue', true);
        AcctSeed__GL_Account__c glAccount2 = TestUtils.createGLAccount('test gl2', 'Revenue', false);
        AcctSeed__GL_Account__c glAccount3 = TestUtils.createGLAccount('test gl3', 'Expense', false);
        AcctSeed__GL_Account__c glAccount4 = TestUtils.createGLAccount('test gl4', 'Balance Sheet', false);
        TestUtils.createGLAccounts();
        AcctSeed__Accounting_Variable__c glVariable = TestUtils.createAccountingVariable('1');
        AcctSeed__Accounting_Variable__c glVariable2 = TestUtils.createAccountingVariable('2');
        AcctSeed__Accounting_Variable__c glVariable3 = TestUtils.createAccountingVariable('3');
        AcctSeed__Accounting_Variable__c glVariable4 = TestUtils.createAccountingVariable('4');

        Account acct = TestUtils.createAccount(glVariable.id);

        AcctSeed__GL_Account__c GL= new AcctSeed__GL_Account__c(name='123', AcctSeed__Type__c='Expense', AcctSeed__Sub_Type_1__c='Labor');
        insert GL;

        AcctSeed__GL_Account__c GL1= new AcctSeed__GL_Account__c(name='124', AcctSeed__Type__c='Balance Sheet',AcctSeed__Sub_Type_1__c='Assets');
        insert GL1;

        AcctSeed__GL_Account__c GL2= new AcctSeed__GL_Account__c(name='125', AcctSeed__Type__c='Revenue');
        insert GL2;


        AcctSeed__Accounting_Period__c ap = [Select id from AcctSeed__Accounting_Period__c where AcctSeed__Status__c = 'Open' Order by AcctSeed__Start_Date__c desc limit 1];//new AcctSeed__Accounting_Period__c();
//        ap.Name = '2019-12';
//        ap.AcctSeed__Start_Date__c = system.today().addDays(-7);
//        ap.AcctSeed__End_Date__c = system.today().addDays(30);
//        ap.AcctSeed__Status__c = 'Open';
//        insert ap;

        Id pricebookId = Test.getStandardPricebookId();


        AcctSeed__Billing_Format__c bf = new AcctSeed__Billing_Format__c();
        bf.AcctSeed__Type__c = 'Activity Statement';
        bf.AcctSeed__Visualforce_PDF_Page__c = 'BillingActivityStatementPDF';
        bf.AcctSeed__Default_Email_Template__c = 'Activity_Statement_Email_Template';

        insert bf;

        //AcctSeed__Ledger__c ledger2 = TestUtils.createLedger(pricebookid,'Test Ledger',glAccount.Id,bf.id);
       // insert ledger2;
       
       AcctSeed__Ledger__c q =[SELECT Id FROM AcctSeed__Ledger__c WHERE AcctSeed__Type__c='Transactional'];
       AcctSeed__Ledger__c ledger2=q;

        //Create Test products
        Map<String,Product2> prods = TestUtils.createProducts(glAccount2.id, glAccount3.id, glAccount4.id);

        AcctSeedERP__Warehouse__c wh = [SELECT Id FROM AcctSeedERP__Warehouse__c WHERE Name='Virtual Warehouse' ];
        //insert wh;

        List<AcctSeedERP__Location__c> locs = new List<AcctSeedERP__Location__c>();
        locs.add(new AcctSeedERP__Location__c(Name = 'Finished Goods', AcctSeedERP__Warehouse__c = wh.Id, Magento_Sync__c = true));
        locs.add(new AcctSeedERP__Location__c(Name = 'NVIS Main Office', AcctSeedERP__Warehouse__c = wh.Id, Magento_Sync__c = true));
        insert locs;

        AcctSeedERP__Inventory_Balance__c bal1 = new AcctSeedERP__Inventory_Balance__c(AcctSeedERP__Warehouse__c = wh.Id, AcctSeedERP__Product__c = prods.get('Test Part').id, AcctSeedERP__Location__c = locs[0].Id, AcctSeedERP__Ledger__c=ledger2.id);
        insert bal1;

        //AcctSeedERP__Inbound_Inventory_Movement__c ibm = new AcctSeedERP__Inbound_Inventory_Movement__c(AcctSeedERP__Credit_GL_Account__c=GL2.id,AcctSeedERP__Unit_Cost__c=12,AcctSeedERP__Inventory_Balance__c = bal1.Id, AcctSeedERP__Type__c = 'Accounting', AcctSeedERP__Quantity__c = 10, AcctSeedERP__Ledger__c=ledger2.id);
        //insert ibm;

       // AcctSeedERP__Outbound_Inventory_Movement__c obm = new AcctSeedERP__Outbound_Inventory_Movement__c(AcctSeedERP__Debit_GL_Account__c=GL2.id,AcctSeedERP__Unit_Cost__c=12,AcctSeedERP__Inventory_Balance__c = bal1.Id, AcctSeedERP__Type__c = 'Accounting', AcctSeedERP__Quantity__c = 10, AcctSeedERP__Ledger__c=ledger2.id,AcctSeedERP__GL_Account_Variable_1__c=glVariable.id,AcctSeedERP__GL_Account_Variable_2__c=glVariable2.id, AcctSeedERP__GL_Account_Variable_3__c=glVariable3.id);
        //insert obm;

        /*

        AcctSeedERP__Purchase_Order__c po = new AcctSeedERP__Purchase_Order__c(AcctSeedERP__Purchase_Order_Format__c = bf.id,AcctSeedERP__Ledger__c = ledger2.id,AcctSeedERP__Order_Date__c = date.today().addDays(-7), AcctSeedERP__Vendor__c = acct.Id);
        insert po;

        AcctSeedERP__Purchase_Order_Line__c pol = new AcctSeedERP__Purchase_Order_Line__c(AcctSeedERP__Unit_Price__c=12, AcctSeedERP__Purchase_Order__c = po.Id, AcctSeedERP__Quantity__c = 10, AcctSeedERP__Product__c = prods.get('Test Part').id, AcctSeedERP__GL_Account_Variable_2__c=glVariable2.id,AcctSeedERP__GL_Account_Variable_3__c=glVariable3.id);
        insert pol;
        */

        AcctSeedERP__Purchase_Order__c po = TestUtils.createPurchaseOrder();
        insert po;

        // Create the Product
        AcctSeed__GL_Account__c glAccountR= TestUtils.createGLAccount('Test glAccount','Revenue',True);
        AcctSeed__GL_Account__c glAccountE= TestUtils.createGLAccount('Test glAccount','Expense',True);
        AcctSeed__GL_Account__c glAccountI= TestUtils.createGLAccount('Test glAccount','Balance Sheet',True);
        Product2 prod= TestUtils.createProduct(glAccountR.id,glAccountE.id,glAccountI.id,true);

        List<AcctSeedERP__Purchase_Order_Line__c> polList = new List<AcctSeedERP__Purchase_Order_Line__c>();
        // Create the PO Line(s)
        String glav2=TestUtils.createAccountingVariable('2').id;
        String glav3=TestUtils.createAccountingVariable('3').id;
        String poid= po.id;
        String prodid= prod.id;
        AcctSeedERP__Purchase_Order_Line__c pol=TestUtils.createPOLine(poid,prodid,5,glav2,glav3);
        insert pol;
        
        AcctSeedERP__Inventory_Balance__c iqa= TestUtils.createIQA(prod);
        //AcctSeedERP__Purchase_Order_Inventory_Movement__c poim = new AcctSeedERP__Purchase_Order_Inventory_Movement__c(AcctSeedERP__Inventory_Balance__c=bal1.id, AcctSeedERP__Unit_Cost__c=100.00, AcctSeedERP__Purchase_Order_Line__c=pol.id, AcctSeedERP__Movement_Date__c=date.today().addDays(-7), AcctSeedERP__Quantity__c=10,AcctSeedERP__GL_Account_Variable_1__c=glVariable.id,AcctSeedERP__GL_Account_Variable_2__c=glVariable2.id,AcctSeedERP__GL_Account_Variable_3__c=glVariable3.id);//new AcctSeedERP__Purchase_Order_Inventory_Movement__c();
        List<AcctSeedERP__Purchase_Order_Inventory_Movement__c> poimList=new List<AcctSeedERP__Purchase_Order_Inventory_Movement__c>();
        for (Integer i = 0; i < 45; i++) {
                AcctSeedERP__Purchase_Order_Inventory_Movement__c poim = new AcctSeedERP__Purchase_Order_Inventory_Movement__c();
                poim.AcctSeedERP__Inventory_Balance__c = iqa.id;
                poim.AcctSeedERP__Unit_Cost__c = 10;
                poim.AcctSeedERP__Purchase_Order_Line__c = pol.Id;
                poim.AcctSeedERP__Movement_Date__c = date.today().addDays(-7);
                poim.AcctSeedERP__Quantity__c = 10;
                poim.AcctSeedERP__GL_Account_Variable_1__c=glVariable.id;
                poim.AcctSeedERP__GL_Account_Variable_2__c=glav2;
                poim.AcctSeedERP__GL_Account_Variable_3__c=glav3;
                poimList.add(poim);
                // AcctSeedERP__Purchase_Order_Inventory_Movement__c poim2=poim.clone();
                // insert poim2;
                // AcctSeedERP__Purchase_Order_Inventory_Movement__c poim3=poim.clone();
                // insert poim3;

                // System.assertEquals(prod.Catalog_Available_Qty__c,30);
                // AcctSeedERP__Purchase_Order_Inventory_Movement__c poim4=poim.clone();
                // insert poim4;
                // AcctSeedERP__Purchase_Order_Inventory_Movement__c poim5=poim.clone();
                // insert poim5;
                //delete poim2;
        }
        insert poimList;     
        // Test.startTest();
        // ID jobId=Database.executeBatch(new InventoryMovementBatchable(poimList,'Purchase Order',false),45);
        // Test.stopTest();
    }

    static void testPOIMTriggerHandler_isDelete() {

    }
}